  
# BairesDev Android Application Example  
  
## Develop an Android application that meets the following requirements:  
  
 1. The design should be a main screen with a side menu type "Hamburger" (Standard Menu)    
    
 2. Must run on devices with android 19 onwards    
    
 3. The side menu will have 3 options, each option changes the contents of the main screen.    
    
 4. The main screen will be changed using fragments or custom views.    
    
 5. The activity can have only one container with a single element, besides the menu.    
    
 6. The application will only have one Activity.    
    
### Menu options:    
1. Google: The site must be loaded into the application: www.google.com  
  
    
2. Buttons: A button that shows a toast and another that shows an "alert" message.    
    
3. A list of the first 10 “Android” projects using kotlin as language in github, using the github API.  
    - Clicking an item must open the project in any browser.  
  
  
### Improvements that can be done in the future:  
  
1. Migrate to AndroidX libraries  
   Google recently has change their libraries with AndroidX initiative  
2. Add Tests  
   - **Espresso** can be used to validate the UI  
   - Since we used **Retrofit** to perform the GitHub API requests, we can use [MockWebServer](https://github.com/square/okhttp/tree/master/mockwebserver) from square to Mock the GitHub API responses to test the application.  
   - Use **Dagger** to improve the testability of the application  
  
### This project have a solution to use Retrofit on Android PreLollipop  
For more information please check [RetrofitHelper](https://bitbucket.org/TinMeM/bairesdev_android/src/master/app/src/main/java/com/bettin/bairesdev_android/data/retrofit/RetrofitHelper.java)