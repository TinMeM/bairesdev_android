package com.bettin.bairesdev_android;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AppExecutors {

    private static AppExecutors sInstance;
    private final Executor networkIO;

    private AppExecutors(Executor networkIO) {
        this.networkIO = networkIO;
    }

    public static AppExecutors getInstance() {
        synchronized (AppExecutors.class) {
            if (sInstance == null) {
                sInstance = new AppExecutors(Executors.newSingleThreadExecutor());
            }
        }
        return sInstance;
    }

    public Executor networkIO() {
        return networkIO;
    }
}
