package com.bettin.bairesdev_android;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.bettin.bairesdev_android.ui.WebView.WebViewFragment;
import com.bettin.bairesdev_android.ui.buttons.ButtonFragment;
import com.bettin.bairesdev_android.databinding.MainActivityBinding;
import com.bettin.bairesdev_android.ui.github.GithubFragment;

public class MainActivity extends AppCompatActivity
        implements FragmentManager.OnBackStackChangedListener {

    MainActivityBinding mLayoutBind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLayoutBind = DataBindingUtil.setContentView(this, R.layout.main_activity);

        setNavigationListener();

        setSupportActionBar(mLayoutBind.mainToolbar);
        getSupportFragmentManager().addOnBackStackChangedListener(this);

        shouldDisplayHomeUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // if not showing any fragment open the options list
                if (!isShowingAnyFragment()) {
                    mLayoutBind.mainDrawerLayout.openDrawer(GravityCompat.START);
                } else {
                    onBackPressed();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Check if the WebView has any back operation before to execute the back operation on the
     * fragments
     */
    @Override
    public void onBackPressed() {
        Fragment fragment =
                getSupportFragmentManager().findFragmentById(mLayoutBind.mainContainer.getId());
        if (fragment != null) {
            if (fragment instanceof WebViewFragment) {
                if (((WebViewFragment) fragment).hasPageToReturn()) return;
            }
            // uncheck the menu option
            mLayoutBind.mainNavigation.getCheckedItem().setChecked(false);
        }

        super.onBackPressed();
    }

    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeUp();
    }

    private void shouldDisplayHomeUp() {
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            if (isShowingAnyFragment()) {
                actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
            } else {
                actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
            }
        }
    }

    private boolean isShowingAnyFragment() {
        return getSupportFragmentManager().getBackStackEntryCount() > 0;
    }

    private void setNavigationListener() {
        mLayoutBind.mainNavigation.setNavigationItemSelectedListener(
                menuItem -> {
                    menuItem.setChecked(true);

                    mLayoutBind.mainDrawerLayout.closeDrawers();

                    switch (menuItem.getItemId()) {
                        case R.id.item_button_frag:
                            openFragment(ButtonFragment.newInstance(), ButtonFragment.TAG );
                            break;

                        case R.id.item_google_frag:
                            openNetworkRelatedFragment(WebViewFragment.newInstance(),
                                    WebViewFragment.TAG);
                            break;

                        case R.id.item_github_frag:
                            openNetworkRelatedFragment(GithubFragment.newInstance(),
                                    GithubFragment.TAG);
                            break;
                    }

                    return true;
                });
    }

    private void openNetworkRelatedFragment(Fragment fragment, String tag) {
        if (isNetworkAvailable()) {
            openFragment(fragment, tag);
        } else {
            showNetworkNotAvailableMessage();
        }
    }

    private void openFragment(Fragment fragment, String tag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(mLayoutBind.mainContainer.getId(), fragment)
                .addToBackStack(tag)
                .commit();
    }

    private boolean isNetworkAvailable() {
        boolean result = false;
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm != null) {
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

            if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {
                result = true;
            }
        }

        return result;
    }

    private void showNetworkNotAvailableMessage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(R.string.network_dialog_message)
                .setTitle(R.string.network_dialog_issue);

        builder.setPositiveButton(R.string.close, (dialog, id) -> dialog.dismiss());

        builder.create().show();
    }

}
