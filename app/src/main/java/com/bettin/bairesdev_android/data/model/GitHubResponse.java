package com.bettin.bairesdev_android.data.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GitHubResponse implements JsonDeserializer<GitHubResponse> {

    private final static String JSON_ELEMENT_COUNT = "total_count";
    private final static String JSON_ELEMENT_INCOMPLETE_RESULTS = "incomplete_results";
    private final static String JSON_ELEMENT_ITENS = "items";

    @SerializedName(JSON_ELEMENT_COUNT) int count;
    @SerializedName(JSON_ELEMENT_INCOMPLETE_RESULTS) String incomplete;
    @SerializedName(JSON_ELEMENT_ITENS) List<Repository> repositories;

    public GitHubResponse() { }

    private GitHubResponse(JsonObject json) {

        this.count = getAsInt(json, JSON_ELEMENT_COUNT);
        this.incomplete = getAsString(json, JSON_ELEMENT_INCOMPLETE_RESULTS);
        this.repositories = getItensList(json, JSON_ELEMENT_ITENS);
    }

    @Override
    public GitHubResponse deserialize(JsonElement jsonElement, Type type,
                                  JsonDeserializationContext jsonDeserializationContext)
            throws JsonParseException {
        return new GitHubResponse(jsonElement.getAsJsonObject());
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getIncomplete() {
        return incomplete;
    }

    public void setIncomplete(String incomplete) {
        this.incomplete = incomplete;
    }

    public List<Repository> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }

    private String getAsString(JsonObject object, String elementName) {
        if (object.has(elementName) ) {
            return object.get(elementName).getAsString();
        }
        return "";
    }

    private int getAsInt(JsonObject object, String elementName) {
        if (object.has(elementName) ) {
            return object.get(elementName).getAsInt();
        }
        return -1;
    }

    private List<Repository> getItensList(JsonObject json, String elementName) {
        List<Repository> listaItens = new ArrayList<>();

        JsonArray repositories = json.getAsJsonArray(elementName);
        for (JsonElement item : repositories) {
            listaItens.add(new Repository(item.getAsJsonObject()));
        }

        return listaItens;
    }
}
