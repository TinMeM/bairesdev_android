package com.bettin.bairesdev_android.data.model;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;

public class Repository implements JsonDeserializer<Repository> {
    private final static String JSON_ELEMENT_NAME = "name";
    private final static String JSON_ELEMENT_FULLNAME = "full_name";
    private final static String JSON_ELEMENT_HTMLURL = "html_url";

    @SerializedName(JSON_ELEMENT_NAME) String name;
    @SerializedName(JSON_ELEMENT_FULLNAME) String fullname;
    @SerializedName(JSON_ELEMENT_HTMLURL) String htmlUrl;

    public Repository() { }

    Repository(JsonObject json) {

        this.name = getAsString(json, JSON_ELEMENT_NAME);
        this.fullname = getAsString(json, JSON_ELEMENT_FULLNAME);
        this.htmlUrl = getAsString(json, JSON_ELEMENT_HTMLURL);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    @Override
    public Repository deserialize(JsonElement jsonElement, Type type,
                                      JsonDeserializationContext jsonDeserializationContext)
            throws JsonParseException {
        return new Repository(jsonElement.getAsJsonObject());
    }

    private String getAsString(JsonObject object, String nome) {
        if (object.has(nome) ) {
            return object.get(nome).getAsString();
        }
        return "";
    }
}
