package com.bettin.bairesdev_android.data.retrofit;

import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;

import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * This class was created to fix an issue with Android PreLollipop
 * The issue is related to the TLS version that android will use, on PreLollipop devices
 * we should force OkHTTP to use TLS 1.2 and not the default one (which is not supported)
 * for more information: https://github.com/square/okhttp/issues/2372#issuecomment-244807676
 *
 * This fixed the issue while testing with emulator
 * on some real devices we may need to use Google API in order to install the latest TLS
 * https://stackoverflow.com/questions/45877125/enable-tls-1-2-in-android-4-4
 */
public class RetrofitHelper {

    public static Retrofit getRetrofitInstance(String url, Gson gson) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit;
    }

    private static OkHttpClient getOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        return enableTls12OnPreLollipop(builder).build();
    }

    private static OkHttpClient.Builder enableTls12OnPreLollipop(OkHttpClient.Builder client) {
        // we are supporting Android from SDK 19, so this is intended for Android 19 to 21
        // AKA 4.4 and 5.0
        if (Build.VERSION.SDK_INT < 22) {
            try {
                SSLContext sc = SSLContext.getInstance("TLSv1.2");
                sc.init(null, null, null);
                client.sslSocketFactory(new Tls12SocketFactory(sc.getSocketFactory()));

                ConnectionSpec cs = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                        .tlsVersions(TlsVersion.TLS_1_2)
                        .build();

                List<ConnectionSpec> specs = new ArrayList<>();
                specs.add(cs);
                specs.add(ConnectionSpec.COMPATIBLE_TLS);
                specs.add(ConnectionSpec.CLEARTEXT);

                client.connectionSpecs(specs);
            } catch (Exception exc) {
                Log.e("OkHttpTLSCompat", "Error while setting TLS 1.2", exc);
            }
        }

        return client;
    }
}
