package com.bettin.bairesdev_android.data.service;

import com.bettin.bairesdev_android.data.model.GitHubResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GitHubService {
    @GET("repositories?q=language:kotlin+topic:android+stars:>=100")
    Call<GitHubResponse> listRepos();
}
