package com.bettin.bairesdev_android.data.source;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.os.Build;
import android.util.Log;

import com.bettin.bairesdev_android.AppExecutors;
import com.bettin.bairesdev_android.data.model.GitHubResponse;
import com.bettin.bairesdev_android.data.model.Repository;
import com.bettin.bairesdev_android.data.service.GitHubService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.bettin.bairesdev_android.data.retrofit.RetrofitHelper.getRetrofitInstance;

public class GitHubDS {
    private static final String BASE_URL = "https://api.github.com/search/";

    private static final Object LOCK = new Object();
    private static GitHubDS sInstance;
    private MediatorLiveData<List<Repository>> mRepositoryList;
    private GitHubService mApiService;

    private GitHubDS() {
        mRepositoryList = new MediatorLiveData<>();
        mRepositoryList.setValue(null);

        mApiService = getGitHubService();
    }

    public static GitHubDS getInstance() {
        synchronized (LOCK) {
            if (sInstance == null) {
                sInstance = new GitHubDS();
            }
        }
        return sInstance;
    }

    public LiveData<List<Repository>> getRepositories() {

        AppExecutors.getInstance().networkIO().execute(() -> {
            // TODO PLACE API CALL HERE

            mApiService.listRepos().enqueue(new Callback<GitHubResponse>() {
                @Override
                public void onResponse(Call<GitHubResponse> call,
                                       Response<GitHubResponse> response) {
                    if (response.isSuccessful()) {
                        GitHubResponse gitResponse = response.body();

                        if (gitResponse != null) {
                            List<Repository> repositories = gitResponse.getRepositories();
                            if ( repositories != null && repositories.size() > 10) {
                                mRepositoryList.postValue(
                                        gitResponse.getRepositories().subList(0, 10));
                            } else {
                                mRepositoryList.postValue(gitResponse.getRepositories());
                            }
                        }
                    } else {
                        Log.d("GitHubDS", "Code: " + response.code() + "Message: " + response.message());
                    }
                }

                @Override
                public void onFailure(Call<GitHubResponse> call, Throwable t) {
                    Log.d("GitHubDS", "getRepositories: Couldn't get the repository data");
                }
            });
        });

        return mRepositoryList;
    }

    private GitHubService getGitHubService() {
        Retrofit retrofit = getRetrofitInstance(BASE_URL, getGson());

        return retrofit.create(GitHubService.class);
    }

    private Gson getGson() {
        return new GsonBuilder()
                .registerTypeHierarchyAdapter(GitHubResponse.class, new GitHubResponse())
                .create();
    }
}
