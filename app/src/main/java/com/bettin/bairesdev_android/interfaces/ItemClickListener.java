package com.bettin.bairesdev_android.interfaces;

import android.view.View;

public interface ItemClickListener<T> {
    void onClick(View view, T object);
}
