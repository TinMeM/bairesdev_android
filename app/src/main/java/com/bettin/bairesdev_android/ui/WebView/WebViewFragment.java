package com.bettin.bairesdev_android.ui.WebView;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.bettin.bairesdev_android.databinding.FragmentWebviewBinding;


public class WebViewFragment extends Fragment {

    public static final String TAG = WebViewFragment.class.getSimpleName();
    private FragmentWebviewBinding mLayoutBind;

    public WebViewFragment() {
        // Required empty public constructor
    }

    public static WebViewFragment newInstance() {
        return new WebViewFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mLayoutBind = FragmentWebviewBinding.inflate(inflater, container, false);

        return mLayoutBind.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();

        prepareWebView();
    }

    /**
     * If there is any navigation history on webview, return to it before execute the
     * onBackPressed on MainActivity
     * @return <code>true</code> if there a back operation was performed
     * <br><code>false</code> if the main there is no back operation to perform
     */
    public boolean hasPageToReturn() {
        boolean result = false;
        if (mLayoutBind.wvContainer.canGoBack()) {
            mLayoutBind.wvContainer.goBack();
            result = true;
        }
        return result;
    }

    private void prepareWebView() {
        mLayoutBind.wvContainer.setWebViewClient(new CustomWebViewClient());
        mLayoutBind.wvContainer.setWebChromeClient(new CustomWebChromeClient());

        WebSettings webSettings = mLayoutBind.wvContainer.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        mLayoutBind.wvContainer.setVerticalScrollBarEnabled(false);
        mLayoutBind.wvContainer.loadUrl("https://www.google.com");
    }

    private class CustomWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            view.loadUrl(request.getUrl().toString());
            return true;
        }

    }

    private class CustomWebChromeClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);

            if (newProgress == 100) {
                mLayoutBind.frameLayout.setVisibility(View.GONE);
                return;
            }

            mLayoutBind.frameLayout.setVisibility(View.VISIBLE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                mLayoutBind.webviewProgress.setProgress(newProgress, true);
            } else {
                mLayoutBind.webviewProgress.setProgress(newProgress);
            }
        }
    }

}
