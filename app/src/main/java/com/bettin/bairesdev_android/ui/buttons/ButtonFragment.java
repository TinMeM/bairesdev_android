package com.bettin.bairesdev_android.ui.buttons;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bettin.bairesdev_android.R;
import com.bettin.bairesdev_android.databinding.FragmentButtonBinding;


public class ButtonFragment extends Fragment {

    public static final String TAG = ButtonFragment.class.getSimpleName();

    private FragmentButtonBinding mLayoutBind;

    public static ButtonFragment newInstance() {
        return new ButtonFragment();
    }

    public ButtonFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        mLayoutBind = FragmentButtonBinding.inflate(inflater, container, false);
        return mLayoutBind.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();

        mLayoutBind.toastButton.setOnClickListener(v ->
                Toast.makeText(getContext(), R.string.toast_example,
                        Toast.LENGTH_LONG).show());

        mLayoutBind.dialogButton.setOnClickListener(v -> openDialog());
    }

    /**
     * Will open the example dialog with a OK button to close
     */
    private void openDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(R.string.dialog_message)
                .setTitle(R.string.dialog_title);

        builder.setPositiveButton(R.string.close, (dialog, id) -> dialog.dismiss());

        builder.create().show();
    }
}
