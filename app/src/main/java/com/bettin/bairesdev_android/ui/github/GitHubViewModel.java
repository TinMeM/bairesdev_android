package com.bettin.bairesdev_android.ui.github;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.ViewModel;

import com.bettin.bairesdev_android.data.model.Repository;
import com.bettin.bairesdev_android.data.source.GitHubDS;

import java.util.List;

public class GitHubViewModel extends ViewModel {

    private final MediatorLiveData<List<Repository>> mRepositoriesList;
    private boolean mIsInitiated;

    public GitHubViewModel() {
        mRepositoriesList = new MediatorLiveData<>();
        mRepositoriesList.setValue(null);

        mIsInitiated = false;
    }

    LiveData<List<Repository>> getRepositories() {

        if (!mIsInitiated) {
            LiveData<List<Repository>> repositoryList =
                    GitHubDS.getInstance().getRepositories();

            mRepositoriesList.addSource(repositoryList,mRepositoriesList::setValue);

            mIsInitiated = true;
        }

        return mRepositoriesList;
    }
}
