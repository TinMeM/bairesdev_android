package com.bettin.bairesdev_android.ui.github;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bettin.bairesdev_android.data.model.Repository;
import com.bettin.bairesdev_android.databinding.FragmentGithubListBinding;
import com.bettin.bairesdev_android.interfaces.ItemClickListener;

public class GithubFragment extends Fragment implements ItemClickListener {

    public static final String TAG = GithubFragment.class.getSimpleName();

    private GitHubViewModel mViewModel;
    private GithubRecyclerViewAdapter mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public GithubFragment() {
    }

    public static GithubFragment newInstance() {
        return new GithubFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentGithubListBinding layout =
                FragmentGithubListBinding.inflate(inflater, container, false);

        mViewModel = ViewModelProviders.of(this)
                .get(GitHubViewModel.class);

        layout.githubList.setLayoutManager(new LinearLayoutManager(getContext()));

        mAdapter = new GithubRecyclerViewAdapter(null, this);
        layout.githubList.setAdapter(mAdapter);

        return layout.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mViewModel.getRepositories().observe(this,
                repositoryList -> mAdapter.setRepositoryList(repositoryList));


    }

    @Override
    public void onClick(View view, Object object) {
        if (object instanceof Repository) {
            Intent i = new Intent(Intent.ACTION_VIEW,
                    Uri.parse(((Repository) object).getHtmlUrl()));
            startActivity(i);
        }
    }
}
