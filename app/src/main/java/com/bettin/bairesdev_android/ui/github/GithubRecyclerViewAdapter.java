package com.bettin.bairesdev_android.ui.github;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bettin.bairesdev_android.data.model.Repository;
import com.bettin.bairesdev_android.interfaces.ItemClickListener;
import com.bettin.bairesdev_android.databinding.FragmentGithubBinding;

import java.util.List;


public class GithubRecyclerViewAdapter
        extends RecyclerView.Adapter<GithubRecyclerViewAdapter.ViewHolder> {

    private final ItemClickListener mListener;
    private List<Repository> mValues;

    public GithubRecyclerViewAdapter(List<Repository> items, ItemClickListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        FragmentGithubBinding mLayoutBind =
                FragmentGithubBinding.inflate(LayoutInflater.from(parent.getContext()), parent,
                        false);

        return new ViewHolder(mLayoutBind);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.bind(getItem(position), mListener);
    }

    @Override
    public int getItemCount() {
        if (mValues == null) return 0;
        return mValues.size();
    }

    public void setRepositoryList(List<Repository> repositoryList) {
        mValues = repositoryList;
        notifyDataSetChanged();
    }

    private Repository getItem(int posicao) {
        return mValues.get(posicao);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final FragmentGithubBinding layoutBind;

        public ViewHolder(FragmentGithubBinding bind) {
            super(bind.getRoot());

            layoutBind = bind;
        }

        @Override
        public String toString() {
            return layoutBind.getRepository().getName();
        }

        /**
         * Binds the needed data do the interface
         * @param repository git hub repository
         * @param listener listener that will handle repository clicks
         */
        public void bind(Repository repository, ItemClickListener listener) {
            layoutBind.setRepository(repository);
            layoutBind.setItemClickListener(listener);

        }
    }
}
